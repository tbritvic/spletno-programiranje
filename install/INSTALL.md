Installation instructions
These are instructions for installation for Microsoft Windows operating system.
1.	First install Node.js from: https://nodejs.org/en/
2.	Install Mongo Data Base from: https://www.mongodb.com/download-center?jmp=nav#community
3.	Download project from: https://bitbucket.org/tbritvic/spletno-programiranje/src
4.	Open command prompt and go to directory in which you have project 
5.	Enter: $ npm install
6.	Enter: $ npm start
With this in your primary web browser the web application should open.
