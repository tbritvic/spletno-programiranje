How to see delete previously added articles 
===========================================

To delete previously added articles you need to:

#) login

#) click My RSS Feeds 

#) choose the article you want to delete

#) click the Delete button 
