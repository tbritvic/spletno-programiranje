Welcome to RSS Article open source site

This a web application that enables users to come and read and rss articles, to put their url's of article.

The code is writen in Node.js. 
Database that is used is MongoDB.
The framework used is Mongoose.
The app itself can be found on: http://web.studenti.math.pmf.unizg.hr/~tbritvi/
The code is in folder \app.
Project contains instructions for installation which can be found in \install.
Project also offers user's guide which can be found in \install\User's guide. 